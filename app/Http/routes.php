<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$controller = 'HomeController@';

Route::get('/', $controller . 'index');
Route::post('register', $controller . 'register');
Route::post('login', 'AuthController@login');
Route::get('logout', 'AuthController@logout');

Route::group(['prefix' => '/', 'middleware' => 'auth'], function() {

	$controller = 'HomeController@';

	Route::get('profile/{id}', $controller . 'profile');
	Route::post('subscribefee', $controller . 'setSubscribeFee');
	Route::get('follow/{id}', $controller . 'followUser');

	Route::get('portfolio', 'PortfolioController@getPortfolio'); // verðbréfasafn
	Route::post('stock', 'PortfolioController@addStock');

	// Route::get('profile', $controller . 'profile'); // um user, vörslusafn þarna inni
	Route::get('market', $controller . 'market');  // upplýsingar um markaðinn
	Route::get('mynetwork', $controller . 'mynetwork'); //tengslanet
	Route::get('topinvestors', $controller . 'topinvestors'); // mest successful fjárfestar
	Route::get('home', $controller . 'home'); // news feed fake stuff

	Route::get('cardbalance', $controller . 'getCardBalance');
	Route::get('addmoney', 'CardController@makeSale');

	Route::get('registerCard', 'CardController@registerCard');

	Route::post('buyStock', 'CardController@buyStocks');
	Route::post('buyBond', 'CardController@buyBonds');
	Route::post('sellShares', 'CardController@sellShares');

});
