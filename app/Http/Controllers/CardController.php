<?php

namespace App\Http\Controllers;

use App\Libs\CurlWrapper;
use App\User;
use Illuminate\Http\Request;
use App\Models\UserAccounts;
use App\Models\StockHistory;
use Auth;

class CardController extends Controller {

	private $valitor;

	public function __construct() {
		//$this->valitor = new CurlWrapper('https://fintech.valitor.is:444/PaymentManager');
	}

	public function registerCard(Request $request) {

		// $cardnumber = $request->input('cardnumber');

		$cardnumber = env('CREDIT_CARD');
		$user = Auth::user();

		if (UserAccounts::where('user_id', '=', $user->id)->exists()) {
			return $this->json(1, 'Notandi hefur nú þegar skráð reikning á þennan aðgang.');
		}

		$account = UserAccounts::create([
			'user_id' => $user->id,
			'card_number' => $cardnumber
		]);

		if( $account->save() )
			return $this->json(0, 'Skráning á reikningi tókst.');
		else
			return $this->json(1, 'Mistókst að skrá reikning.');

	}

	public function buyBonds(Request $request) {

		$user = Auth::user();

		$account = UserAccounts::where('user_id', $user->id)->first();

		if( !isset($account) )
			return $this->json(1, 'Enginn reikningur er skráður. Þú verður að skrá reikning áður en þú kaupir bréf.');

		$shareAmount = $request->bondsAmount;
		if ( $shareAmount <= 0 ) {
			return $this->json(1, 'Fjöldi bréfa þarf að vera að minnsta kosti 1.');
		}
		$data = $request->bondsData;

		$amountBought = intval($shareAmount*$data['BuyPrice']);

		$headers[] = 'Content-Type: application/json';

		//$response = $this->valitor->sendRequest('/MakePayment/' . $account->card_number . '/' . $amountBought . '/ISK/flemann', $headers, 'post');

		//if( $response != null ) {
			$userProfile = StockHistory::create([
				'user_id' => $user->id,
				'initial_rate' => $data['BuyPrice'],
				'stock_name' => $data['Name'],
				'stock_type' => 'bond',
				'share_count' => $shareAmount,
				'symbol' => $data['Symbol'],
			]);
			return $this->json(0, 'Kaup á bréfum tókst. Þú keyptir ' . $shareAmount . ' skuldabréf fyrir ' . number_format($amountBought, 0, '.','.') . ' ISK.');
		//}
		//else
		//	return $this->json(1, 'Ekki tókst að kaupa bréf. Reyndu aftur síðar.');

	}

	public function buyStocks(Request $request) {

		$user = Auth::user();

		$account = UserAccounts::where('user_id', $user->id)->first();

		if( !isset($account) )
			return $this->json(1, 'Enginn reikningur er skráður. Þú verður að skrá reikning áður en þú kaupir bréf.');

		$shareAmount = $request->stockAmount;
;
		if ( $shareAmount <= 0 ) {
			return $this->json(1, 'Fjöldi bréfa þarf að vera að minnsta kosti 1.');
		}
		$data = $request->stockData;

		$amountBought = intval($shareAmount*$data['BuyPrice']);

		$headers[] = 'Content-Type: application/json';

		//$response = $this->valitor->sendRequest('/MakePayment/' . $account->card_number . '/' . $amountBought . '/ISK/flemann', $headers, 'post');

		//if( $response != null ) {
			$userProfile = StockHistory::create([
				'user_id' => $user->id,
				'initial_rate' => $data['BuyPrice'],
				'stock_name' => $data['Name'],
				'stock_type' => 'stock',
				'share_count' => $shareAmount,
				'symbol' => $data['Symbol'],
			]);
			return $this->json(0, 'Kaup á bréfum tókst. Þú keyptir ' . $shareAmount . ' hlutabréf fyrir ' . number_format($amountBought, 0, '.','.') . ' ISK.');
		//}
		//else
		//	return $this->json(1, 'Ekki tókst að kaupa bréf. Reyndu aftur síðar.');

	}

	public function makePayment(Request $request) {

		$amount = $request->input('amount');
		$user = Auth::user();

		$account = UserAccounts::where('user_id', $user->id)->first();

		if( !isset($account) )
			return $this->json(1, 'Enginn reikningur er skráður.');

		$headers[] = 'Content-Type: application/json';

		//$response = $this->valitor->sendRequest('/MakePayment/' . $account->card_number . '/' . $amount . '/ISK/flemann', $headers, 'post');
		$response = 3;
		if( $response != null )
			return $this->json(0, 'Greiðsla tókst.');
		else
			return $this->json(1, 'Greiðsla mistókst.');

	}

	public function makeSale(Request $request) {

		$amount = $request->input('amount');
		$user = Auth::user();

		$account = UserAccounts::where('user_id', $user->id)->first();

		if( !isset($account) )
			return $this->json(1, 'Enginn reikningur er skráður.');

		$headers[] = 'Content-Type: application/json';

		//$response = $this->valitor->sendRequest('/AddFundsToCard/' . $account->card_number . '/' . $amount, $headers, 'post');
		$response = 3;
		if( $response != null )
			return $this->json(0, 'Sala tókst.');
		else
			return $this->json(1, 'Sala mistókst.');

	}

}
