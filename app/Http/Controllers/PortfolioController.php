<?php

namespace App\Http\Controllers;

use App\Libs\CurlWrapper;
use App\User;
use Illuminate\Http\Request;
use App\Models\UserAccounts;
use App\Models\StockHistory;
use Auth;
use stdClass;

class PortfolioController extends Controller {

    public function getPortfolio() {

        $user = Auth::user();
		$stocks = StockHistory::where('user_id', $user->id)->get();

        $skuldabref = array();
        $hlutabref = array();

        foreach ( $stocks as $stock ) {
            if ( $stock->stock_type == 'stock' ) {
                $temp = new stdClass();

                $shareCount = $stock->share_count;
                $stockValue = intval($shareCount*$stock->initial_rate);
                $skip = false;
                foreach ( $hlutabref as $bref ) {
                    if ( $bref->CompanyName == $stock->stock_name) {
                        $bref->Shares += $shareCount;
                        $bref->TotalValue += $stockValue;
                        $skip = true;
                    }
                }
                if ( $skip ) continue;
                $temp->TotalValue = $stockValue;
                $temp->CompanyName = $stock->stock_name;
                $temp->CompanySymbol = $stock->symbol;
                $temp->Shares = $stock->share_count;
                array_push($hlutabref, $temp);
            } else if ( $stock->stock_type == 'bond' ) {
                $temp = new stdClass();

                $shareCount = $stock->share_count;
                $stockValue = intval($shareCount*$stock->initial_rate);
                $skip = false;
                foreach ( $skuldabref as $bref ) {
                    if ( $bref->CompanyName == $stock->stock_name) {
                        $bref->Shares += $shareCount;
                        $bref->TotalValue += $stockValue;
                        $skip = true;
                    }
                }
                if ( $skip ) continue;
                $temp->TotalValue = $stockValue;
                $temp->CompanyName = $stock->stock_name;
                $temp->CompanySymbol = $stock->symbol;
                $temp->Shares = $stock->share_count;
                array_push($skuldabref, $temp);
            }
        }

        return view('portfolio')->with('hlutabref', $skuldabref)
                                ->with('skuldabref', $hlutabref);
    }

}
