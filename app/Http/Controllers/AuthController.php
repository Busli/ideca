<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Libs\CurlWrapper;

class AuthController extends Controller {

	private $arion;
	private $kodi;

	public function __construct() {
		$this->arion = new CurlWrapper('https://arionapi-sandbox.azure-api.net');
	}

	public function login(Request $request) {

		$input = $request->input();

		$user = User::where('username', $input['username'])->first();

		if ( isset($user) && $user->verified == 0 ) {
			$verified = $this->checkIfVerified($user);

			if ( $verified == 'unverified' ) {
				return $this->json(1, 'Notandi hefur ekki verið virkjaður. Þú þarft að samþykkja skilmálana sem sendir voru á ' . $user->email );
			} else if ( $verified == 'error' ) {
				return $this->json(1, 'Ekki tókst að sækja upplýsingar um samþykkt skilmála. Reyndu aftur síðar.');
			} else if ( $verified == 'verified' ) {
				$user->verified = 1;

				if ( !$user->save() ) {
					return $this->json(1, 'Ekki tókst að ná sambandi við gagnagrunn. Reyndu aftur síðar.');
				}
			}
		}

		if( Auth::attempt(['username' => $input['username'], 'password' => $input['password']]) )
			return $this->json(0, 'OK', url('home'));
		else
			return $this->json(1, 'Ekki tókst að skrá þig inn.');

	}

	public function checkIfVerified($user) {

		$headers = array();
		$headers[] = 'Content-Type: application/json';
		$headers[] = 'Ocp-Apim-Subscription-Key: ' . env('ARION_KEY', '12345');
		$headers[] = 'Authorization: Bearer ' . env('JWT_TOKEN', '12345');

		$response = $this->arion->sendRequest('/digitalsignatures/v1/digitalSignatures/' . $user->uuid, $headers);

		if ( $response != null) {
			$response = json_decode($response);

			if ( $response->digitalSignatureRequestStatus != 'signed' ) {
				return 'unverified';
			}
		} else {
			return 'error';
		}

		return 'verified';

	}

	public function postLogin(Request $request) {

		$input = $request->input();

		if( Auth::attempt(['username' => $input['username'], 'password' => $input['password']]) )
			return redirect()->to('/');
		else
			return $this->json(1, 'Error came up when logging in');

	}

	public function logout() {

		Auth::logout();
		return redirect()->to('');

	}

}
