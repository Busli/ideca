<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\UserProfile;
use App\Models\UserRelations;
use App\Models\UserAccounts;
use App\Models\StockHistory;
use Hash, Auth, Log, stdClass;
use App\Libs\CurlWrapper;
use App\Libs\Guid;

class HomeController extends Controller {

	private $arion;
	private $kodi;
	private $valitor;

	public function __construct() {
		$this->arion = new CurlWrapper('https://arionapi-sandbox.azure-api.net');
		$this->valitor = new CurlWrapper('https://fintech.valitor.is:444/PaymentManager');
	}

	public function index() {

		return view('index');

	}

	// ****** TABS BY ÓTTAR ******
	public function profile($id) {

		// $username = Auth::user()->username;

		$user = User::where('id', $id)->first();
		$userProfile = ($user->id == Auth::user()->id) ? true : false;

		$followerCount = UserRelations::where('user_friend_id', $user->id)
							->where('following', 1)
							->get()->count();

		$userData = UserProfile::where('ssn', $user->ssn)->first();

		$thoseIfollow = UserRelations::where('user_id', $user->id)
							->where('following', 1)
							->get();

		$fee = 0;
		foreach( $thoseIfollow as $follower ) {

			$followingSsn = User::select('ssn')
							->where('id', $follower->user_friend_id)
							->first();

			$fee += UserProfile::select('subscribe_fee')
							->where('ssn', $followingSsn->ssn)->first()->subscribe_fee;

		}

		$stocksValue = $this->calculateStocksFee($user);
		$bondsValue = $this->calculateBondsFee($user);

		$data_hlutabref = number_format($stocksValue/($stocksValue+$bondsValue)*100, 2);
		$data_skuldabref = number_format($bondsValue/($stocksValue+$bondsValue)*100, 2);

		return view('profile')
				->with('user_hlutabref',$data_hlutabref)
				->with('user_skuldabref',$data_skuldabref)
				->with('subFee', $userData->subscribe_fee)
				->with('user', $user)
				->with('stocksValue', $stocksValue)
				->with('bondsValue', $bondsValue)
				->with('balance', $this->getCardBalance())
				->with('gjold', $fee)
				->with('tekjur',$followerCount*$userData->subscribe_fee)
				->with('userProfile', $userProfile)
				->with('subscribeFee', $userData->subscribe_fee)
				->with('userData', $userData);

	}

	public function calculateStocksFee($user) {

		$stocks = StockHistory::where('user_id', $user->id)->get();

		if ( $stocks->isEmpty() ) {
			return 1;
		}

		$totalSum = 0;
		foreach ( $stocks as $stock ) {
			if ( $stock->stock_type == 'stock') {
				$totalSum += ($stock->initial_rate)*($stock->share_count);
			}
		}
		return $totalSum;

	}

	public function calculateBondsFee($user) {

		$stocks = StockHistory::where('user_id', $user->id)->get();

		if ( $stocks->isEmpty() ) {
			return 1;
		}

		$totalSum = 0;
		foreach ( $stocks as $stock ) {
			if ( $stock->stock_type == 'bond') {
				$totalSum += ($stock->initial_rate)*($stock->share_count);
			}
		}

		return $totalSum;

	}

	public function getCardBalance() {

		$user = Auth::user();
		$account = UserAccounts::where('user_id', $user->id)->first();

		if( !isset($account) ) {
			return -1;
		}

		$headers = array();
		$headers[] = 'Content-Type: application/json';

		return 325328;
		//$response = $this->valitor->sendRequest('/CardBalance/' . $account->card_number, $headers);

		//if( $response != null )
		//	return $response;
		//else
		//	return 0;

	}

	public function market()
	{
		return view('market');
	}

	public function home()
	{
		return view('home');
	}

	public function mynetwork() {

		$user = Auth::user();
		$iIsFollowing = UserRelations::where('user_id', $user->id)
							->where('following', 1)
							->get();

		foreach( $iIsFollowing as $follow ) {

			$followingUsername = User::select('username')
							->where('id', $follow->user_friend_id)
							->first();

			$follow->username = $followingUsername->username;
			$follow->LastMovement = StockHistory::select('updated_at')->where('user_id', $follow->user_friend_id)->first()['updated_at'];

		}

		$iIsBeingFollowed = UserRelations::where('user_friend_id', $user->id)
								->where('following', 1)
								->get();

		foreach( $iIsBeingFollowed as $follower ) {

			$followerUsername = User::select('username')
							->where('id', $follower->user_id)
							->first();

			$follower->username = $followerUsername->username;

		}

		return view('mynetwork')
			->with('iIsFollowing', $iIsFollowing)
			->with('iIsBeingFollowed', $iIsBeingFollowed);
	}

	public function topinvestors() {


		$user = Auth::user();
		$users = User::where('id', '!=', $user->id)->get();
		$low = 85000;
		$high = 90000;
		$gain = Rand($low,$high);

		foreach( $users as $user ) {


			$data = UserProfile::where('ssn', $user->ssn)->first();
			$user->userData = $data;
			$low = $low - 5000;
			$high = $high - 5000;
			$gain = Rand($low,$high);
		}

		return view('topinvestors')
			->with('topInvestor', $users)->with('gainer',$gain);

	}

		// ****** END OF TABS BY ÓTTAR ******

	public function storeUserDetails($ssn) {

		$profile = $this->getSsnDetails($ssn);

		if ( $profile == 'Not found') {
			return 'Kennitala ekki á skrá.';
		} else if ( $profile == 'error' ) {
			return 'Villa kom upp við að sækja upplýsingar frá Þjóðskrá.';
		}

		if( $profile->Fate == 1 )
			return 'Notandi er því miður látinn.';

		$userProfile = UserProfile::create([
			'ssn' => $profile->Kennitala,
			'name' => $profile->FullName,
			'address' => $profile->Home,
			'city' => $profile->City,
			'postal_code' => $profile->PostalCode,
		]);

		if( $userProfile->save() )
			return 'success';
		else
			return 'Villa kom upp við að vista notandaupplýsingar.';

	}

	public function register(Request $request) {

		$input = $request->input();

		if (User::where('username', '=', $input['username'])->exists()) {
			return $this->json(1, 'Notanandafn er ekki laust.');
		}

		if (User::where('ssn', '=', $input['ssn'])->exists()) {
			return $this->json(1, 'Kennitala hefur nú þegar verið skráð.');
		}

		$uuid = $this->createSignature($request->input());

		if ( $uuid == 'error' ) {
			return $this->json(1, 'Villa kom upp við að búa til staðfestingarform.');
		}

		$documentResult = $this->sendDocument($uuid);

		if ( $documentResult == 'error' ) {
			return $this->json(1, 'Villa kom upp við að senda staðfestingarform.');
		}

		$userProfile = $this->storeUserDetails($input['ssn']);

		if( $userProfile != 'success' )
			return $this->json(1, $userProfile);

		$user = User::create([
			'username' => $input['username'],
			'email' => $input['email'],
			'password' => Hash::make($input['password']),
			'ssn' => $input['ssn'],
			'uuid' => $uuid,
		]);

		if( $user->save() )
			return $this->json(0, 'OK', url('profile/' . $user->id ));
		else
			return $this->json(1, 'Villa kom upp við að stofna notanda.');

	}

	public function createSignature($user) {

		$headers = array();
		$headers[] = 'Content-Type: application/json';
		$headers[] = 'Ocp-Apim-Subscription-Key: ' . env('ARION_KEY', '12345');
		$headers[] = 'Authorization: Bearer ' . env('JWT_TOKEN', '12345');

		$method = 'post';
		$uuid = Guid::create();

		$data = new stdClass();
		$data->displayTitle = "Sign me please";
		$data->uuid = $uuid;
		$data->visibleDescription = "You need to sign this document to continue, yo";
		$data->signaturesShouldBeOrderedByGroups = false;
		$data->group = array();
		$data->group[0] = new stdClass();
		$data->group[0]->isOptional = false;
		$data->group[0]->signee = array();
		$data->group[0]->signee[0] = new stdClass();
		$data->group[0]->signee[0]->email = $user['email'];
		$data->group[0]->signee[0]->kennitala = $user['ssn'];
		$data->group[0]->signee[0]->name = $user['username'];

		$response = $this->arion->sendRequest('/digitalsignatures/v1/digitalSignatures/', $headers, $method, json_encode($data));

 		if ( $response != null ) {
			return $uuid;
		} else {
			return 'error';
		}
	}

	public function sendDocument($uuid) {

		$headers = array();
		$headers[] = 'Content-Type: text/plain';
		$headers[] = 'Ocp-Apim-Subscription-Key: ' . env('ARION_KEY', '12345');
		$headers[] = 'Authorization: Bearer ' . env('JWT_TOKEN', '12345');

		$pdfPath = storage_path('app') . '/chicken.pdf';

		$data = base64_encode(file_get_contents($pdfPath));

		$method = 'post';

		$response = $this->arion->sendRequest('/digitalsignatures/v1/digitalSignatures/' . $uuid . '/documentContent', $headers, $method, $data);

 		if ( $response != null ) {
			return 'success';
		} else {
			return 'error';
		}

	}

	public function getSsnDetails($ssn) {

		$curl = curl_init();

		$headers = array();
		$headers[] = 'Content-Type: application/json';
		$headers[] = 'Ocp-Apim-Subscription-Key: ' . env('ARION_KEY', '12345');

		$response = $this->arion->sendRequest('/nationalregistry/v1/nationalRegistryParty/' . $ssn, $headers);

		if( $response != null ) {
			if ( $response == 'null' ) {
				return 'Not found';
			} else {
				return json_decode($response);
			}
		} else {
			return 'error';
		}

	}

	public function setSubscribeFee(Request $request) {

		$subscribeFee = $request->input('subscribeFee');

		$user = Auth::user();
		$userProfile = UserProfile::where('ssn', $user->ssn)->first();

		$userProfile->subscribe_fee = $subscribeFee;

		if( $userProfile->save() )
			return $this->json(0, 'Verð hefur verið uppfært.');
		else
			return $this->json(1, 'Villa kom upp við að vista verð.');

	}

	public function followUser($id) {

		// TODO: User can not follow the same user twice

		$user = Auth::user();
		$userToFollow = User::where('id', $id)->first();

		if (UserRelations::where('user_id', $user->id)->where('user_friend_id', $id)->exists()) {
			return $this->json(1, 'Þú ert nú þegar að fylgja þessum notanda.');
		}

		$following = UserRelations::create([
			'user_id' => $user->id,
			'user_friend_id' => $userToFollow->id,
			'following' => 1
		]);

		if( $following->save() )
			return $this->json(0, 'Þú ert núna að fylgja notanda');
		else
			return $this->json(1, 'Villa kom upp við að fylgja notanda.');

	}

}
