<?php

namespace App\Snail\Cache;

use Carbon\Carbon;
use File;

class SnailCache extends \Illuminate\Cache\CacheManager {

	private $cacheKey = 'cacheManager_flusher';
	private $cacheTime = 180;
	private $directory;

	public function __construct() {

		parent::__construct(app());

	}

	/**
     * Override the function
     *
     * @param  array  $config
     * @return \Illuminate\Cache\FileStore
     */
    protected function createFileDriver(array $config)
    {
        return $this->repository(new \Illuminate\Cache\FileStore($this->app['files'], $this->directory));
    }

    public function check() {

    	$this->directory = config('cache.stores.file.path');

		if(!$this->has($this->cacheKey)) {

			$this->flushCache();
			$this->generateCacheFlusher();

		}

	}

	public function flushCache() {

		$this->flush();

	}

	public function generateCacheFlusher() {

		$this->directory = config('cache.stores.file.path');

		$this->put($this->cacheKey, 'Not flushed', Carbon::now()->addWeeks(1));

	}

	public function makeCacheDirectory() {

		File::makeDirectory($this->generateCachePath(), $mode = 0777, true, true);

	}

	public function generateCachePath() {

		return config('cache.stores.file.path') . '/';

	}

	public function putCache($key, $value) {

		$this->directory = $this->generateCachePath();

		$this->put($key, $value, $this->cacheTime);

	}

	public function getCache($key) {

		$this->directory = $this->generateCachePath();

		$store = $this->stores[config('cache.default')];

		return json_decode($store->get($key));

	}

	public function hasCache($key) {

		$this->directory = $this->generateCachePath();

		if($this->has($key))
			return true;

		return false;

	}

}