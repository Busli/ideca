<?php

namespace App\Snail;

use Config;
use App\Snail\Cache\SnailCache;

class Snail {

	protected $api;
	protected $version = '';
	protected $base;
	protected $debug;
	protected $debugData;
	protected $requestData;

	private $response = null;
	private $url = null;
	private $contentType = 'application/json';	// Set to JSON as default
	private $token = null;
	private $cache;

	/**
	*	__construct
	*
	*	@param 	object 	$settings
	*
	*	If user wants different settings than in the config he can pass a settings object to the construct
	*
	**/
	public function __construct( $settings = null ) {

		$this->api = (isset($settings->api)) ? $settings->api : Config::get('snail.api');
		$this->version = (isset($settings->version)) ? $settings->version : Config::get('snail.version');
		$this->debug = (isset($settings->debug)) ? $settings->debug : Config::get('snail.debug');
		$this->debugData = (isset($settings->debugData)) ? $settings->debugData : Config::get('snail.debugData');

		$this->base = $this->api . '/' . (!empty($this->version) ? $this->version . '/' : '');

		$this->cache = new SnailCache();

	}

	/**
	*	log
	*
	*	@param 	string 	$filename
	*	@param 	string 	$responseData
	*
	*	Function that logs the request and the response and saves it to file with the date.
	*	If debug is on then the response is logged.
	*	If debugData is on then the request is also logged.
	*
	**/
	private function log( $fileName, $responseData ) {

		if( $this->debug ) {

			$path = app_path() . '/Snail/debug/';

			try {

				if( is_dir($path) ) {

					$fileName .= '-' . date('Y-m-d-H-i-s') . '.json';

					$file = fopen( $path . $fileName, 'w' );

					if( $this->debugData && is_string($this->requestData)) {
						fwrite( $file, "REQUEST DATA:\r\n");
						fwrite( $file, $this->requestData . "\r\n");
						fwrite( $file, "RESPONSE:\r\n");
					}

					fwrite( $file, $responseData );
					fclose( $file );

				}

			} catch(Exception $ex) {
				// Do not let the library break cause of write issues
			}

		}

	}

	/**
	*	generateSendHeaders
	*
	*	@param 	array 	$headers
	*	@return $sendHeaders
	*
	*	Function that prepares all the headers to be sent
	*
	**/
	private function generateSendHeaders( $headers ) {

		$sendHeaders = array();

		if(isset($headers['Content-Type'])) {
			$this->contentType = $headers['Content-Type'];
		} else {
			// Default
			$headers['Content-Type'] = $this->contentType;
		}

		if(isset($headers[ Config::get('snail.session-token-name') ])) {
			$this->token = $headers[ Config::get('snail.session-token-name') ];
		}

		foreach($headers as $k => $v) {
			$sendHeaders[] = "$k: $v";
		}

		return $sendHeaders;

	}

	/**
	*	request
	*
	*	@param 	string 	$method
	*	@param 	array 	$data
	*	@param 	array 	$headers
	*
	*	Function that makes the actual request. The payload is stored in the $data variable and
	*	headers in the $headers variable. The function supports get, post, put and delete.
	*	$data must be a json string
	*
	**/
	private function request( $method = 'get', $data = null, $headers = null ) {

		$curl = curl_init();

		$this->requestData = $data;

		if( $method == 'get' ) {

			curl_setopt($curl, CURLOPT_URL, $this->base . $this->url);

		} else if ( $method == 'post' ) {

			curl_setopt($curl, CURLOPT_URL, $this->base . $this->url);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

		} else if ( $method == 'put' || $method == 'delete' ) {

			curl_setopt($curl, CURLOPT_URL, $this->base . $this->url);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, strtoupper($method));

			if($data != null)
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

		}

		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, Config::get('snail.connection-timeout'));
		curl_setopt($curl, CURLOPT_TIMEOUT, Config::get('snail.request-timeout'));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);		// Return the response instead of print

		$sendHeaders = $this->generateSendHeaders($headers);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $sendHeaders);


		if(!($this->response = curl_exec($curl))) {

			$this->response = null;				// Set to null if request fails

		} else {

			if($this->debug) {

				$method = strtoupper($method);
				$url = urlencode($this->url);

				$this->log(($method . '-' . $url), $this->response);
			}

		}

		curl_close($curl);

		return $this;

	}

	/**
	*	processRequest
	*
	*	@param 	string	$url
	*	@param 	array 	$options
	*	@param 	string 	$callback
	*	@param 	string	$method
	*
	*	Function to process all inputs so the request can understand it. If request is cached the cache is returned.
	*	$url contains what needs to be added to the base
	*	$options contains the data payload and all headers
	*	$callback contains a string with the name of the callback function
	*	$method contains which method to execute
	*
	**/
	public function processRequest( $url, $options, $method = 'get', $callback = null ) {

		$this->url = $url;

		$data = (isset($options['data'])) ? $options['data'] : null;
		$headers = (isset($options['headers'])) ? $options['headers'] : null;

		if(is_string($data))
			$data = trim($data);

		if($method == 'get' && $data != null)
			$this->url = $url . '?' . http_build_query($data);

		// If request is cached then return cache
		if(!$this->cache->hasCache($this->url)) {

			$this->request( $method, $data, $headers );

		} else {

			// Return the cache
			return $this->cache->getCache($this->url);

		}

		if( $this->response == null ) {
			// Error in request
			throw new SnailException(strtoupper($method) . ' Request: ' . $this->base . $this->url . ' failed', 1);
		}

		if( $callback != null && is_callable($callback) )
			return call_user_func($callback, json_decode($this->response));

		// Return the expected data or return the whole response
		if(isset($options['expect'])) {

			$expect = $options['expect'];

			if($this->contentType == 'application/json') {

				$json = json_decode($this->response);

				if(isset($json->$expect)) {
					return $json->$expect;
				}

				return $json;

			}

		} else {

			return json_decode($this->response);

		}
	}

	/**
	*	cache
	*	
	*	@param null
	*
	*	When called the response from the last request is cached
	*
	**/
	public function cache() {

		if( $this->response != null && $this->url != null ) {
			$this->cache->putCache( $this->url, $this->response );
		}

	}

	/**
	*	getCurlValueForFile
	*
	*	@param 	file 	$file
	*	@return curl value for file
	*
	*	Function takes in file and gets the curl value so the file can be sent with POST method.
	*
	**/
	public function getCurlValue($file) {
    
		$filename = realpath($file);
		$contentType = $file->getClientMimeType();
		$postname = $file->getClientOriginalName();

	    // PHP 5.5 introduced a CurlFile object that deprecates the old @filename syntax
	    // See: https://wiki.php.net/rfc/curl-file-upload
	    if (function_exists('curl_file_create')) {
	        return curl_file_create($filename, $contentType, $postname);
	    }
	 
	    // Use the old style if using an older version of PHP
	    $value = "@{$this->filename};filename=" . $postname;
	    if ($contentType) {
	        $value .= ';type=' . $contentType;
	    }
	 
	    return $value;
	}

	/**
	*	get
	*
	*	@param 	string	$url
	*	@param 	array 	$options
	*	@param 	string 	$callback
	*	@return response from get request
	*
	**/
	public function get($url, $options, $callback = null) {

		return $this->processRequest( $url, $options, 'get', $callback );

	}

	/**
	*	post
	*
	*	@param 	string	$url
	*	@param 	array 	$options
	*	@param 	string 	$callback
	*	@return response from post request
	*
	**/
	public function post($url, $options, $callback = null) {

		return $this->processRequest( $url, $options, 'post', $callback );

	}

	/**
	*	put
	*
	*	@param 	string	$url
	*	@param 	array 	$options
	*	@param 	string 	$callback
	*	@return response from put request
	*
	**/
	public function put($url, $options, $callback = null) {

		return $this->processRequest( $url, $options, 'put', $callback );

	}

	/**
	*	delete
	*
	*	@param 	string	$url
	*	@param 	array 	$options
	*	@param 	string 	$callback
	*	@return response from delete request
	*
	**/
	public function delete($url, $options, $callback = null) {

		return $this->processRequest( $url, $options, 'delete', $callback );

	}

}