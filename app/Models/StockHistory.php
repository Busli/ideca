<?php

namespace App\Models;

use Eloquent;

class StockHistory extends Eloquent {

	protected $table = 'stock_history';
	protected $guarded = ['id'];

}
