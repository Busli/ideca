<?php

namespace App\Models;

use Eloquent;

class UserRelations extends Eloquent {

	protected $table = 'userRelations';
	protected $guarded = ['id'];
	protected $hidden = ['created_at', 'updated_at'];

}
