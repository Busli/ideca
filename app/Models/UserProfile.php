<?php

namespace App\Models;

use Eloquent;

class UserProfile extends Eloquent {

	protected $table = 'userProfile';
	protected $guarded = ['id'];
	protected $hidden = ['created_at', 'updated_at'];

}
