<?php

namespace App\Models;

use Eloquent;

class UserAccounts extends Eloquent {

	protected $table = 'userAccounts';
	protected $guarded = ['id'];
	protected $hidden = ['created_at', 'updated_at'];

}
