<?php

namespace App\Libs;

class CurlWrapper {

	private $baseUrl;

	public function __construct($url) {

		$this->baseUrl = $url;

	}

	public function sendRequest( $url, $headers, $method = 'get', $data = null ) {

		$curl = curl_init();

		$sendUrl = $this->baseUrl . $url;

		curl_setopt($curl, CURLOPT_URL, $sendUrl);

		if( $method == 'post' ) {
		    curl_setopt($curl, CURLOPT_POST, true);
		    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		}

		if( $method == 'put' || $method == 'delete' ) {
		    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, strtoupper($method));

		    if($data != null)
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

		}

		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($curl, CURLOPT_TIMEOUT, 30);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($curl);

		curl_close($curl);

		return ($result) ? $result : null;

	}

}
