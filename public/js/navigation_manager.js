
var _menuHidden;

$( document ).ready(function() {
  initNavigationClock();
});

_menuHidden = false;

$("#extenderID").click(function()
{
  var _scale, _opacity, _color,_skew,_height;
  if(!_menuHidden)
  {
    _scale = 0.0;
    _opacity = 0.0;
    _color = 'white';
    _skew = "180deg";
    _height = '0%';
  }
  else
  {
    _scale = 1.0;
    _opacity = 1.0;
    _color = '#B7E2C7';
    _skew = '360deg';
    _height = '100%';
  }
  TweenLite.to($(".navLinks"), 1, {scale:_scale, opacity:_opacity});
  TweenLite.to($(".navRightPane"), 1, { backgroundColor:_color});
  TweenLite.to($("#extenderID"), 0.5, { skewX: _skew});
  _menuHidden = !_menuHidden;
});


function initNavigationClock() {
    var today = new Date();
    document.getElementById('navigationDate').innerHTML = formatDate(today);
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = navigationclock_checkTime(m);
    s = navigationclock_checkTime(s);
    document.getElementById('navigationClock').innerHTML =
    h + ":" + m + ":" + s;
    var t = setTimeout(initNavigationClock, 500);
}
function navigationclock_checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

function formatDate(date) {
    var d = new Date(date || Date.now()),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [ day,month, year].join('/');
}
