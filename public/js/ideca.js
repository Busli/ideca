;(function(window) {

	window.ideca = {

		model: {},

		init: function() {  },

		bind: function() { ko.applyBindings( this.model ) },

		ajax: function(url, method, data, headers, callbacks ) {

			$.ajax({
				url: url,
				method: method,
				data: data,
				dataType: 'json',
				headers: headers,
				timeout: 15000,
				success: function(data) {

					if( callbacks.success !== undefined )
						callbacks.success(data)

				},
				error: function(resp) {

					if( callbacks.error !== undefined )
						callbacks.error(resp)

				}
			})

		}

	}

	$(function() { ideca.init() })

})(window)