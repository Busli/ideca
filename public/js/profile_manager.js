
;(function(ideca) {

	ideca.ProfileManager = {

		init: function() {
			this.jquery()
		},
		initChart: function() {

			var ctx = $("#myChart");

			var data = {
				labels:
				[
					"Hlutabréf",
					"Skuldabréf"

				],
				datasets:
				[
					{
						data: [$("#pieChartInfo_hlutabref").text(),$("#pieChartInfo_skuldabref").text()],
						backgroundColor:
						[
								"#95CE82",
								"#44212C"
						],
						hoverBackgroundColor:
						[
								"#95CE82",
								"#44212C"
						]
					}
				]
			}

			var myPieChart = new Chart(ctx,{
				type: 'pie',
				data: data
			});

		},
		updateSubscribeFee: function(fee) {

			ideca.ajax('/subscribefee', 'post', { subscribeFee: fee }, null, {
				success: function(data) {
					console.log('data', data)
					vex.dialog.alert(data.message)
				},
				error: function(resp) {
					console.error(resp)
				}
			})

		},
		jquery: function() {

			var self = this

			$(".button.follow").click(function(event) {

				var _getButtonValue = this.innerHTML.split(' ')[2],
					innerSelf = this

				vex.dialog.confirm({
					message: 'Viltu fylgjast með viðskiptum ' + this.value + ' fyrir '+ $(this).data('fee') +' kr á mánuði?',
					callback: function(value) {
						if( value )
							self.followUser($(innerSelf).data('id'))
					}
				});

			});

			$("#user_followerFee").click(function() {
				vex.dialog.buttons.YES.text = 'Staðfesta fylgjendagjald';
				vex.dialog.buttons.NO.text = 'Hætta við';
				vex.dialog.prompt({
					message: 'Hvað viltu rukka notendur fyrir að fylgjast með viðskiptum þínum?',
					placeholder: '(ISK)',
					callback: function(value) {
						if (value) {
							self.updateSubscribeFee(value);
							window.location = window.location;
						}
					}
				})
			});

			$("#user_cardRegister").click(function() {
				vex.dialog.buttons.YES.text = 'Staðfesta reikningsnúmer';
				vex.dialog.buttons.NO.text = 'Hætta við';
				vex.dialog.prompt({
					message: 'Sláðu inn reikningsnúmer í reitinn hér fyrir neðan',
					placeholder: 'Reikningsnúmer',
					callback: function(value) {
						if (value) {
							ideca.ajax('/registerCard/', 'get', null, null, {
								success: function(data) {
									vex.dialog.buttons.YES.text = 'OK';
									vex.dialog.alert(data.message);
									window.location = window.location;
								},
								error: function(resp) {
									console.error(resp)
								}
							})
						}
					},
				});
			});

		},
		followUser: function(id) {

			ideca.ajax('/follow/' + id, 'get', null, null, {
				success: function(data) {
					console.log('data', data)
					vex.dialog.alert(data.message)
				},
				error: function(resp) {
					console.error(resp)
				}
			})

		}


	}

	$(function() { ideca.ProfileManager.init() })

})(window.ideca)
