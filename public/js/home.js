;(function(ideca) {

	ideca.Home = {

		m: ideca.model,
		form: $('form'),

		init: function() {

			this.form.on('submit', this.handleFormSubmit)
			this.initModel(this.m, function() {
				ideca.bind()
			})

		},
		initModel: function(m, callback) {

			m.registering = ko.observable(false)
			m.loginText = ko.observable('Nýr notandi')
			m.loginButtonText = ko.observable('Innskráning')
			m.isLoading = ko.observable(false)

			m.loginToggle = function(model) {

				m.registering(!m.registering())
				m.loginText( (!m.registering()) ? 'Nýr notandi' : 'Til baka'  )
				m.loginButtonText( (!m.registering()) ? 'Innskráning' : 'Nýskrá' )

			}

			m.username = ko.observable('')
			m.password = ko.observable('')
			m.ssn = ko.observable('')
			m.email = ko.observable('')

			if( callback !== undefined )
				callback()

		},
		handleFormSubmit: function(event) {

			var self = ideca.Home
			event.preventDefault()
			self.m.isLoading(true)

			if( self.validateData() ) {

				var url = ( self.m.registering() ) ? 'register' : 'login',
					data = {
						username: self.m.username(),
						password: self.m.password()
					}

				if( self.m.registering() ) {
					data.ssn = self.m.ssn(),
					data.email = self.m.email()
				}

				ideca.ajax(url, 'post', data, null, {
					success: function(data) {

						if( data.status == 0 )
							window.location = data.results
						else
							vex.dialog.alert(data.message)

						self.m.isLoading(false)

					},
					error: function(resp) {
						console.error(resp)
						self.m.isLoading(false)
					}
				})

			}

		},
		validateData: function() {

			if( this.m.username() == '' ) {
				vex.dialog.alert('Notendanafn vantar')
				self.m.isLoading(false)
				return false
			}

			if( this.m.password() == '' ) {
				vex.dialog.alert('Lykilorð vantar')
				self.m.isLoading(false)
				return false
			}

			if( this.m.registering() ) {

				if( this.m.ssn() == '' ) {
					vex.dialog.alert('Kennitölu vantar')
					self.m.isLoading(false)
					return false
				}

				if( this.m.email() == '' ) {
					vex.dialog.alert('Netfang vantar')
					self.m.isLoading(false)
					return false
				}

			}

			return true

		}

	}

	$(function() { ideca.Home.init() })

})(window.ideca)
