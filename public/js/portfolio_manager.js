var _stocks = "";
var _bonds = "";

$( document ).ready(function() {
  KodiService.login('ArionFinTech1');

  var _stocksSymbols = $(".skuldabref_symbol");

  fixAsyncSharesProblems(0, _stocksSymbols);

  var _bondsSymbols = $(".hlutabref_symbol");

  fixAsyncBondsProblems(0, _bondsSymbols);


});


function fixAsyncSharesProblems(startIndex, aCompanyObject)
{
  var _symbolKey = aCompanyObject[startIndex].innerHTML.trim();
  KodiService.GetSharesSnapshot({symbol: _symbolKey},function(companyData)
  {
    var _item = companyData[0].last_price;
    _stocks += _item + '^';
    startIndex++;
    if(startIndex < aCompanyObject.length) fixAsyncSharesProblems(startIndex,aCompanyObject);
    else populateShares();
  })};

  function fixAsyncBondsProblems(startIndex, aCompanyObject)
  {
    var _symbolKey = aCompanyObject[startIndex].innerHTML.trim();
    KodiService.GetBondsSnapshot({symbol: _symbolKey},function(companyData)
    {
      var _item = companyData[0].previous_closing_price;
      _bonds += _item + '^';
      startIndex++;
      if(startIndex < aCompanyObject.length) fixAsyncBondsProblems(startIndex,aCompanyObject);
      else populateBonds();
    })};


    function fixData(value)
    {
      return (value == null || value =="" || 0) ? 1 : parseFloat(value);
    }

  function populateShares()
  {
    var _array = _stocks.split('^');
    var stockBonds = $(".skuldabref_shares");

    var stockNow = $(".hlutabref_now");
    var stockMove = $(".hlutabref_move");
    var stockPerc = $(".hlutabref_perc");

    for(var i = 0 ; i < stockBonds.length; i++)
    {
        if(stockNow[i] != undefined)
        {
          stockMove[i].innerHTML = (fixData(_array) * fixData(stockBonds[i].innerHTML.trim()) - fixData(stockNow[i].innerHTML.trim())).toFixed(2);
          if(fixData(stockNow[i].innerHTML.trim()) == 0)
          stockPerc[i].innerHTML = fixData(_array) * fixData(stockBonds[i].innerHTML.trim()) / 1;
          else

            stockPerc[i].innerHTML = (fixData(_array) * fixData(stockBonds[i].innerHTML.trim()) / fixData(stockNow[i].innerHTML.trim())).toFixed(2);

        }
    }

  }

  function populateBonds()
  {
    var _array = _bonds.split('^');

    var _bondStocks = $(".hlutabref_shares");

    var stockNow = $(".skuldabref_now");
    var stockMove = $(".skuldabref_move");
    var stockPerc = $(".skuldabref_perc");

    for(var i = 0 ; i < _bondStocks.length; i++)
    {
      if(_bondStocks[i] != undefined)
      {
        stockMove[i].innerHTML = (fixData(_array) * fixData(_bondStocks[i].innerHTML.trim()) - fixData(stockNow[i].innerHTML.trim())).toFixed(2);
        if(fixData(stockNow[i].innerHTML.trim()) == 0)
        stockPerc[i].innerHTML = fixData(_array) * fixData(_bondStocks[i].innerHTML.trim()) / 1;
        else

          stockPerc[i].innerHTML = (fixData(_array) * fixData(_bondStocks[i].innerHTML.trim()) / fixData(stockNow[i].innerHTML.trim())).toFixed(2);
      }
    }

  }
/*
  KodiService.GetBondsSnapshot({symbol: bondSymbol},function(bondsData)
  {

    var _bondsSymbol = $(".skuldabref_symbol");
    for(var i = 0 ;)
    var _lastPrice = bondsData[0].last_price;
    console.log(_lastPrice)
  });


  function getStuff(anArray)
  {

  }*/
    // KodiService.GetShares({
    //     exchange: exchange,
    //     symbol: symbol
    //   }, function(data)
    //   {
    //     console.log(data);
    //   });
    //
    //   KodiService.GetBonds({
    //         exchange: exchange,
    //         symbol: bondSymbol
    //       }, function(data)
    //       {
    //         console.log(data);
    //       });
