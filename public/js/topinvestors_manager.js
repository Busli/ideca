;(function(ideca) {

	ideca.TopInvestors = {

		m: ideca.model,

		init: function() {

			var _gainValues = $( ".gain_value" ),
				_highestAmount = 1000,
				self = this

			for(var i = 0 ; i < _gainValues.length ; i++) {
				var _amountGained =  _gainValues[i].outerText;
				var _percantageOfBiggestValue = parseFloat(_amountGained / i+4) / _highestAmount + 10;
				_gainValues[i].innerHTML = this.barAsString(_percantageOfBiggestValue, _amountGained);
			}

			$(".follow-button .button").click(function() {

				var _getButtonValue = this.innerHTML.split(' ')[2],
					innerSelf = this

				console.log('clicked')

				vex.dialog.confirm({
					message: 'Viltu fylgjast með viðskiptum ' + this.value + ' fyrir '+_getButtonValue +' kr á mánuði?',
					callback: function(value) {
						if( value )
							self.followUser($(innerSelf).data('id'))
					}
				});

			});

			this.jquery()

		},
		jquery: function() {
			console.log('inside')
			$('.duration-button .button').on('click', function(event) {
				var buttons = $('.button')
				console.log('clicked')
				buttons.each(function(key, button) {
					if( $(button).hasClass('selected') )
						$(button).removeClass('selected')
				})

				$(this).addClass('selected')

			})

		},
		barAsString: function(aPercentage, anAmount) {
			if(!isFinite(aPercentage)) aPercentage = 55 + Math.random() * 9;
			return "<div style='height:30px;color:#fff;padding-left:5px;horizontal-align:middle;width:" + aPercentage + "%; background-color:#4DCDB6'>" + aPercentage.toFixed(2) + "%</div>";
		},
		followUser: function(id) {

			ideca.ajax('/follow/' + id, 'get', null, null, {
				success: function(data) {
					console.log('data', data)
					vex.dialog.alert(data.message)
				},
				error: function(resp) {
					console.error(resp)
				}
			})

		}

	}

	$(function() { ideca.TopInvestors.init() })

})(window.ideca)
