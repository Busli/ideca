// Logging in on server side and providing the JavaScript with a token is much better than providing the JavaScript with a username and password and asking it to take care of the login.
    // Logging in is covered here as an example of how it is done.
    $( document ).ready(function() {

      $(".hlutabref_buttonClick").click(function()
      {

      });

    });

    function formatDate(date) {
        var d = new Date(date || Date.now()),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [ day,month, year].join('/');
    }

    function testGraph(data)
    {
      var _data = [];
      for(var i = 0 ; i < data.length; i++) _data.push(data[i].close);//data.push(data[i].close);
      var ctx = $("#myChart");
      var myChart = new Chart(ctx, {
          type: 'line',
          data:{
            labels: [formatDate(new Date(2016, 0, 1)),formatDate(new Date(2016, 1, 1)),formatDate(new Date(2016, 2, 1)),formatDate(new Date(2016, 3, 1)),formatDate(new Date(2016, 4, 1)),formatDate(new Date(2016, 5, 1))],
            datasets: [
                {
                    label: data[0].symbol,
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: "rgba(75,192,192,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(75,192,192,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: _data,
                }
            ]
        }
      });
    }

    KodiService.login('ArionFinTech1');
    //KodiService.setToken('YOUR TOKEN HERE');

    function Success(source) {
      return function(data)
      {
        _data = data;
        console.debug('error', source,data);
      }
    }

    function Error(source) {
      return function(error) {
        console.debug('error', source, error);
      }
    }

    $(function() {
      var exchange = 'XICE';
      var symbol = 'MARL', bondSymbol = 'HFF150224';
      var fromDate = new Date(2016, 0, 1); // January 1st
      var toDate = new Date();

      // Overall viewmodel for this screen, along with initial state
      function StockCompanyModel()
      {

          var self = this;

          self.displayGraph = function(data)
          {
            var fromDate = new Date(2016, 0, 1); // January 1st
            var toDate = new Date();
            KodiService.GetEndOfDayPrices
            (
              {
                symbol: data.Symbol,
                fromDate: fromDate,
                toDate: toDate
              },function(data)
            {
              setTimeout(function()
              {

                $(".canvasContainer").appendTo($("#placeGraph"));
                $(".canvasContainer").show();
                testGraph(data);
              }
                ,500
              )

              vex.dialog.confirm
              ({
              message:  '<div id="placeGraph"></div>' ,
              callback: function(value)
              {
                $(".canvasContainer").hide();
                $(".canvasContainer").appendTo($("#bla"));
                return console.log('veee');
              }})
})

          };

          self.buyStock = function(data,event)
          {
            vex.dialog.buttons.YES.text = 'Kaupa hlutabréf';
            vex.dialog.buttons.NO.text = 'Hætta við';
            vex.dialog.prompt
            ({
              message: 'Kaupa hlut í ' + data.Name + ' á ' + data.BuyPrice() + ' ' + data.TradingCurrency,
              placeholder: 'Sláðu inn fjölda bréfa hér',
              callback: function(value)
              {
                if (value) {
                    ideca.ajax('/buyStock', 'post', { stockAmount: value, stockData: data }, null, {
                      success: function(data) {
                          vex.dialog.buttons.YES.text = 'OK';
                          vex.dialog.alert(data.message);
                      },
                      error: function(resp) {
                          console.error(resp);
                      }
                  });
              }
              }
            });
          }

          self.buyBond = function(data,event)
          {
            vex.dialog.buttons.YES.text = 'Kaupa skuldabréf';
            vex.dialog.buttons.NO.text = 'Hætta við';
            vex.dialog.prompt
            ({
              message: 'Kaupa hlut í ' + data.Name + ' á ' + data.BuyPrice() + ' ' + data.TradingCurrency,
              placeholder: 'Sláðu inn fjölda bréfa hér',
              callback: function(value)
              {
                  if (value) {
                    ideca.ajax('/buyBond', 'post', { bondsAmount: value , bondsData: data}, null, {
                        success: function(data) {
                            vex.dialog.buttons.YES.text = 'OK';
                            vex.dialog.alert(data.message);
                        },
                        error: function(resp) {
                            console.error(resp);
                        }
                  });
                  }
              }
            });
          }


          self.company = ko.observableArray([]);
          self.bonds = ko.observableArray([]);
          var _data;
          //var companyInfo
          KodiService.GetShares({  exchange: exchange}, function(data)
          {
            for(var i = 0 ; i < data.length; i++)
            {
              var _company = data[i];
              var companyColumns = {};
              companyColumns.Name = _company.Name;
              companyColumns.TradingCurrency = _company.TradingCurrency;
              companyColumns.Symbol = _company.Symbol;
              companyColumns.ClosingPrice = ko.observable("");
              companyColumns.IntraDayChanged = ko.observable("");
              companyColumns.LastPriceDiff = ko.observable("");
              companyColumns.BuyPrice = ko.observable("");
              companyColumns.SalePrice = ko.observable("");
              companyColumns.Volume = ko.observable("");
              companyColumns.Velta = ko.observable("");
              self.company.push(companyColumns);
            }
            fixAsyncSharesProblems(0, self.company());
          });

          KodiService.GetBonds({ exchange: exchange}, function (data)
          {
            for(var i = 0 ; i < 25; i++)
            {
              var _company = data[i];
              var companyColumns = {};
              companyColumns.Name = _company.Name;
              companyColumns.TradingCurrency = _company.TradingCurrency;
              companyColumns.Symbol = _company.Symbol;
              companyColumns.ClosingPrice = ko.observable("");
              companyColumns.IntraDayChanged = ko.observable("");
              companyColumns.LastYield = ko.observable("");
              companyColumns.BuyPrice = ko.observable("");
              companyColumns.SalePrice = ko.observable("");
              companyColumns.Volume = ko.observable("");
              companyColumns.Velta = ko.observable("");
              self.bonds.push(companyColumns);
            }
            fixAsyncBondsProblems(0, self.bonds());
            var companyEmptyHolder = {};
            companyEmptyHolder.Name = "";
            companyEmptyHolder.TradingCurrency = "";
            companyEmptyHolder.Symbol = "";
            companyEmptyHolder.ClosingPrice = "";
            self.bonds.push(companyColumns);
          });
      }

      function fixAsyncSharesProblems(startIndex, aCompanyObject)
      {
        var _companyObject = aCompanyObject[startIndex];
        var _symbolKey = _companyObject.Symbol;
        KodiService.GetSharesSnapshot({symbol: _symbolKey},function(companyData)
        {
          aCompanyObject[startIndex].ClosingPrice(companyData[0].previous_closing_price);
          var _perChange = companyData[0].intraday_per_change != null ? companyData[0].intraday_per_change.toFixed(2) : "";
          aCompanyObject[startIndex].IntraDayChanged(_perChange);
          aCompanyObject[startIndex].LastPriceDiff(companyData[0].last_price_diff);
          aCompanyObject[startIndex].BuyPrice(valueOnFormat(companyData[0].last_price));
          aCompanyObject[startIndex].SalePrice(valueOnFormat(companyData[0].intraday_average_price));
          aCompanyObject[startIndex].Volume(numberWithCommas(valueOnFormatZero(companyData[0].intraday_accumulated_volume)));
          aCompanyObject[startIndex].Velta(numberWithCommas(valueOnFormatZero(companyData[0].intraday_accumulated_turnover)));

          startIndex++;
          if(startIndex < aCompanyObject.length) fixAsyncSharesProblems(startIndex,aCompanyObject);
        });
      }


      function fixAsyncBondsProblems(startIndex, aCompanyObject)
      {
        var _companyObject = aCompanyObject[startIndex];
        var _symbolKey = _companyObject.Symbol;

        KodiService.GetBondsSnapshot({symbol: _symbolKey},function(companyData)
        {
          //console.log(companyData[0]);
          aCompanyObject[startIndex].ClosingPrice(companyData[0].previous_closing_price);
          aCompanyObject[startIndex].IntraDayChanged(valueOnFormat(companyData[0].intraday_per_change));
          aCompanyObject[startIndex].LastYield(valueOnFormat(companyData[0].last_yield));
          aCompanyObject[startIndex].BuyPrice(valueOnFormat(companyData[0].last_price));
          aCompanyObject[startIndex].SalePrice(valueOnFormat(companyData[0].previous_closing_price));
          aCompanyObject[startIndex].Volume(numberWithCommas(valueOnFormatZero(companyData[0].intraday_accumulated_volume)));
          aCompanyObject[startIndex].Velta(numberWithCommas(valueOnFormatZero(companyData[0].intraday_accumulated_turnover)));

          startIndex++;
          if(startIndex < aCompanyObject.length) fixAsyncBondsProblems(startIndex,aCompanyObject);
        });
      }

      function numberWithCommas(x) {
          var parts = x.toString().split(".");
          parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
          return parts.join(".");
      }

            function valueOnFormat(value)
            {
              return value != null ? value.toFixed(2) : "";
            }
            function valueOnFormatZero(value)
            {
              return value != null ? value.toFixed(0) : "";
            }
      /*
      */

      ko.applyBindings(new StockCompanyModel());

    /*  KodiService.GetShares({
        exchange: exchange,
        symbol: symbol
      }, Success('GetShares'), Error('GetShares'));


      KodiService.GetSharesSnapshot({
        symbol: symbol
      }, Success('GetSharesSnapshot'), Error('GetSharesSnapshot'));
*/
      return;

      KodiService.GetAllTradables({
        exchange: exchange
      }, Success('GetAllTradables'), Error('GetAllTradables'));



      KodiService.GetBonds({
        exchange: exchange,
        symbol: bondSymbol
      }, Success('GetBond'), Error('GetBonds'));
          KodiService.GetTradableStatistics({
            symbol: symbol
          }, Success('GetTradableStatistics'), Error('GetTradableStatistics'));

      KodiService.GetBonds({
        exchange: exchange
      }, Success('GetAllBonds'), Error('GetBonds'));


      KodiService.GetEndOfDayPrices({
        symbol: symbol,
        fromDate: fromDate,
        toDate: toDate
      }, Success('GetEndOfDayPrices'), Error('GetEndOfDayPrices'));

      KodiService.GetSharesSnapshot({
        exchange: exchange
      }, Success('GetAllSharesSnapshot'), Error('GetAllSharesSnapshot'));



      KodiService.GetBondsSnapshot({
        exchange: exchange
      }, Success('GetAllBondsSnapshot'), Error('GetAllBondsSnapshot'));

      KodiService.GetBondsSnapshot({
        symbol: bondSymbol
      }, Success('GetBondsSnapshot'), Error('GetBondsSnapshot'));

      KodiService.GetTradableStatistics({
        symbol: symbol
      }, Success('GetTradableStatistics'), Error('GetTradableStatistics'));
    });
