<?php

return [

	/*
	|-------------------------------------------------------------------------------|
	| Snail Options                                                                 |
	|-------------------------------------------------------------------------------|
	|                                                                               |
	|   (string)    Api: Base URL for your API                                  	|
	|   (string)    Version: Used for version switching                            	|
	|   (string)    Session-token-name: The name for your session token            	|
	|   (string)    Connection-timeout: Timeout for connection in seconds 			|
	|	(string)	Request-timeout: Timeout of the request in seconds 				|
	|   (boolean)   Debug: True/False for storing json response in debug folder     |
	|   (boolean)   DebugData: True/False for storing json request in debug log    	|
	|                                                                               |
	|-------------------------------------------------------------------------------|
	*/

	'api' => env('SNAIL_API_URL', 'localhost'),
	'version' => env('SNAIL_VERSION', 'v1'),
	'session-token-name' => env('SNAIL_TOKEN_NAME', ''),
	'connection-timeout' => env('SNAIL_CONNECTION_TIMEOUT', 10),
	'request-timeout' => env('SNAIL_REQUEST_TIMESOUT', 30),
	'debug' => env('SNAIL_DEBUG', false),
	'debugData' => env('SNAIL_DEBUG_DATA', false),

];
