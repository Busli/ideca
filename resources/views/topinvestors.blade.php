@extends('layouts.master')

@section('content')
<div class="top-investors">
	<div class="row top-headline">
		<div class="col-sm-10 title">
			<h3>Farsælustu fjárfestarnir</h3>
		</div>
		<div class="col-sm-8 col-sm-offset-4">
			<ul>
				<li class="">
					{{--<a href="#">Viku</a>--}}
					<div class="duration-button">
						<button class="button selected">Vika</button>
					</div>
				</li>
				<li class="">
					{{--<a href="#">Mánuði</a>--}}
					<div class="duration-button">
						<button class="button">Mánuður</button>
					</div>
				</li>
				<li class="">
					{{--<a href="#">Ár</a>--}}
					<div class="duration-button">
						<button class="button">Ár</button>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<div class="row table-header">
		<div class="col-sm-8">
			<h4 class="text-center">Notendanafn</h4>
		</div>
		<div class="col-sm-8">
			<h4 class="text-center">Hagnaður</h4>
		</div>
		<div class="col-sm-8">
			<h4 class="text-center">Fylgja</h4>
		</div>
	</div>
	@foreach( $topInvestor as $investor )
		<div class="row table-content">
			<div class="col-sm-8">
				<p class="name">{{$investor->username}}</p>
			</div>
			<div class="col-sm-8">
				<p class="gain_value">{{ $gainer }}</p>
			</div>
			<div class="col-sm-5 col-sm-offset-3">
				<div class="follow-button">
					<button data-id="{{$investor->id}}" class="button" value="{{$investor->username}}">Fylgja fyrir {{number_format($investor->userData->subscribe_fee, 0, '.','.')}} ISK</button>
				</div>
			</div>
		</div>
	@endforeach
</div>
@stop

@section('scripts')
<script src="{{asset('js/topinvestors_manager.js')}}" defer></script>
@stop
