@extends('layouts.master')

@section('header')

@stop

@section('content')
	<section class="container">
		<div class="row">
			<div class="col-xs-12 col-xs-offset-2">
				<img src="{{ asset('images/opening_picture.png') }}" alt="'Avatar'" />
			</div>
			@if(!Auth::check())
			<div class="col-xs-9 col-xs-offset-1">
				<form class="login-form" method="post" action="{{url('')}}">
					<div>
						<div class="inline-block">
							<div class="login-row inline-block">
								<label>Notandanafn</label>
								<input type="text" name="username" placeholder="Notandanafn" data-bind="value: username">
							</div>
							<div class="login-row inline-block">
								<label>Lykilorð</label>
								<input type="password" name="password" placeholder="Lykilorð" data-bind="value: password">
							</div>
						</div>
						<div class="inline-block" data-bind="visible: registering">
							<div class="login-row inline-block">
								<label>Kennitala</label>
								<input type="text" name="ssn" placeholder="1122334567" data-bind="value: ssn">
							</div>
							<div class="login-row inline-block">
								<label>Netfang</label>
								<input type="text" name="email" placeholder="jon@jonsson.is" data-bind="value: email">
							</div>
						</div>
					</div>
					<div>
						<div class="submit-button inline-block">
							<button class="button" type="submit" data-bind="css: { disabled: isLoading() }, text: loginButtonText"></button>
						</div>
						<div class="toggle-text inline-block">
							<button class="button" data-bind="text: loginText, click: loginToggle"></button>
						</div>
					</div>
				</form>
			</div>
			@endif
		</div>
	</section>
	<section class="container BrefabraskAbout">
		<h3 class="text-center">Félagsleg verðbréfaviðskipti </h3>

		<h4>Opnar nýjar dyr</h4>
		<p>
		Umfram almenna sölu og kaup á verðbréfum þá gerir verðbréfaviðskiptahugbúnaður með samskiptamiðli fjárfestum kleift að deila, læra, herma og keppa við hvern annan. Aukin tengsl fjárfesta eykur upplýsingaflæði og umfang viðskipta á markaðnum.
		Einfaldleikinn og aðstoðin sem hægt er að fá í gegnum samfélag hugúnaðarins mun auka fjárfestingamöguleika fólks með því að opna betur dyrnar að verðbréfafjárfestingum.</p>

		<h4>Eflir nýgræðinga</h4>
		<p>
		Einstakt tækifæri fyrir einstaklinga sem fjárfesta ekki nægilega hárri upphæð að þeir geta farið og rætt við eignarstýringarfélag né hafa þeir tímann og upplýsingarnar til að greina markaðinn. Þessi tiltekni hópur getur nú fylgst með, lært af og hermt eftir farsælum fjárfestum.
		</p>
		<h4>Eflir hæfileikaríka greiningaraðila</h4>
		<p>
		Góðir greiningaraðilar með hagnýta reynslu munu sjá sér aukinn hag í farsælum fjárfestingum (góðri frammistöðu á verðbréfamarkaði) þar sem þeir geta dregið að sér fylgjendur og þannig aukið tekjur sínar. Birtur er listi um farsælustu fjárfesta síðustu viku/mánaðar/árs og því myndast samkeppni milli fjárfesta sem hafa tímann og upplýsingarnar til að greina markaðinn.
		</p>
		<div >
			<a>
		<img style="display:block;margin:auto" src="{{ asset('images/concept.png') }}" alt="'Avatar'" /></a>
	</div>
		<p class="text-center"><strong>Að „braska“ er auðvelt, félagslegt, lærdómsríkt, og skemmtilegt.</strong></p>
		<p class="text-center"><strong>„Brask“ er fyrir alla.</strong></p>

		<p style="font-size:0.8em">Það er ókeypis að stofna aðgang í BRÉFABRASK. Í upphafi þarf einstaklingur að nýskrá sig og skrifa undir rafrænan samning í síma sínum. Þegar allt hefur verið samþykkt þá fær notandinn DMA-aðgang (e. direct market access) sem er beinn aðgangur að verðbréfamarkaðnum á Íslandi. Þar með getur notandinn sýslað með verðbréf á netinu í símanum og myndað sérvalið tengslanet en notandinn ákveður sjálfur hvaða fjárfestar geta fylgst með honum og hann getur sent beiðni um að fylgjast með öðrum.
		Engin þörf verður á verðbréfaþjónustu Arionbanka, þe. miðlun og ráðgjöf, með tilkomu hugbúnaðarins. Annars vegar verður engin þörf á verðbréfamiðlurum þar sem verðbréfaviðskipti munu verða fýsilegri með DMA-aðgangi í síma hvers fjárfestis og hins vegar verður engin þörf á verðbréfaráðgjöf/eignarstýringu þar sem notendur fá upplýsingar og öðlast reynslu í gegnum tengslanet sitt innan hugbúnaðarins.
		</p>
	</section>
@stop

@section('scripts')
<script src="{{asset('js/home.js')}}"></script>
<script src="{{asset('js/chart_manager.js')}}"></script>
@stop
