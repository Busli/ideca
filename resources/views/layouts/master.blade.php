<?php $user = Auth::user(); ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('css/vendor/vex.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('css/vendor/vex-theme-bottom-right-corner.css')}}">
		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="{{asset('css/vendor/vex-theme-plain.css')}}">
		<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/knockout/3.4.0/knockout-min.js"></script>

		<title>Ideca</title>
		@yield('head')
	</head>
	<body>

		<header class="container-fluid" style="background: #4DCDB6">
			<div class="container">
				@if($user != null)
					@include('partials.nav')
				@endif
				@yield('header')
			</div>
		</header>
			<div class="page_content">
				<div class="container">
					@yield('content')
				</div>
			</div>

		<footer class="container-fluid">
			<div class="container">
				@yield('footer')
			</div>
		</footer>

		<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/knockout/3.4.0/knockout-min.js"></script>
		<script src="{{asset('js/vendor/vex.combined.min.js')}}"></script>
		<script>vex.defaultOptions.className = 'vex-theme-plain';</script>
		<script src="{{asset('js/ideca.js')}}"></script>
		@yield('scripts')

	</body>
</html>
