@extends('layouts.master')


@section('head')
	<link rel="stylesheet" type="text/css" href="{{asset('css/topinvestor.css')}}">
	<script src="{{asset('js/topinvestors_manager.js')}}" defer></script>
@stop

@section('content')
<div class="home">
	<div class="row">
		<div class="col-sm-12 text-center border-right">
			<h2>Viðskiptafréttir</h2>
			<div class="news">
				<div class="head">
					<h4>04.06.2016</h4>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span>Ríkið hagnaðist um 286 milljarða á hruninu</span>
					</div>
					<div class="info col-sm-3">
						<span>DV</span>
					</div>
					<div class="time col-sm-3">
						<span>14:30</span>
					</div>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span>Skipt um stjórn Arev NII slhf.</span>
					</div>
					<div class="info col-sm-3">
						<span>RÚV</span>
					</div>
					<div class="time col-sm-3">
						<span>13:45</span>
					</div>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span>Jákvæður greiðslujöfnuður þjóðarbúsins</span>
					</div>
					<div class="info col-sm-3">
						<span>VB</span>
					</div>
					<div class="time col-sm-3">
						<span>12:54</span>
					</div>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span>Úrvalsvísitala kauphallar Nasdaq Iceland lækkaði um 1,06%</span>
					</div>
					<div class="info col-sm-3">
						<span>VB</span>
					</div>
					<div class="time col-sm-3">
						<span>13:24</span>
					</div>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span>Stefanía nýr framkvæmdarstjóri CCP á Íslandi</span>
					</div>
					<div class="info col-sm-3">
						<span>VB</span>
					</div>
					<div class="time col-sm-3">
						<span>13:01</span>
					</div>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span>Hlutdaild Icelandair minnkar</span>
					</div>
					<div class="info col-sm-3">
						<span>MBL</span>
					</div>
					<div class="time col-sm-3">
						<span>12:14</span>
					</div>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span>Brim hf. kaupir Ögurvík</span>
					</div>
					<div class="info col-sm-3">
						<span>VB</span>
					</div>
					<div class="time col-sm-3">
						<span>11:55</span>
					</div>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span>Síldarvinnsla hagnast um 6,2 milljarða</span>
					</div>
					<div class="info col-sm-3">
						<span>MBL</span>
					</div>
					<div class="time col-sm-3">
						<span>09:58</span>
					</div>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span>Húsnæðisfrumvörp samþykkt á Alþingi</span>
					</div>
					<div class="info col-sm-3">
						<span>VB</span>
					</div>
					<div class="time col-sm-3">
						<span>09:30</span>
					</div>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span>Gróði ríkisins gæti orðið 160 milljarðar</span>
					</div>
					<div class="info col-sm-3">
						<span>Vísir</span>
					</div>
					<div class="time col-sm-3">
						<span>09:25</span>
					</div>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span>Leggur til einkavæðingu Landsvirkjunar</span>
					</div>
					<div class="info col-sm-3">
						<span>RÚV</span>
					</div>
					<div class="time col-sm-3">
						<span>09:10</span>
					</div>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span>Hagstæður viðskiptajöfnuður við útlönd</span>
					</div>
					<div class="info col-sm-3">
						<span>MBL</span>
					</div>
					<div class="time col-sm-3">
						<span>08:44</span>
					</div>
				</div>
				<div class="head">
					<h4>03.06.2016</h4>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span>ORF líftækni hagnast í fyrsta sinn</span>
					</div>
					<div class="info col-sm-3">
						<span>VB</span>
					</div>
					<div class="time col-sm-3">
						<span>21:32</span>
					</div>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span>Frumvarp um fjármagnsinnflæði tefst</span>
					</div>
					<div class="info col-sm-3">
						<span>VB</span>
					</div>
					<div class="time col-sm-3">
						<span>21:15</span>
					</div>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span>Lengsta hagvaxtarskeið í 70 ár?</span>
					</div>
					<div class="info col-sm-3">
						<span>VB</span>
					</div>
					<div class="time col-sm-3">
						<span>20:04</span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12 text-center">
			<h2>Viðskipti fylgjenda</h2>
			<div class="news">
				<div class="head">
					<h4>04.06.2016</h4>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span><span class="red">Jón Þór</span> keypti hlutabréf í <span class="red">Marel</span> fyrir 1 milljón króna</span>
					</div>
					<div class="info col-sm-3"></div>
					<div class="time col-sm-3">
						<span>15:07</span>
					</div>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span><span class="red">Óttar</span> keypti hlutabréf í <span class="red">Marorku</span> fyrir 2 milljón króna</span>
					</div>
					<div class="info col-sm-3"></div>
					<div class="time col-sm-3">
						<span>14:07</span>
					</div>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span><span class="red">Óttar</span> keypti hlutabréf í <span class="red">Vodafone</span> fyrir 2,5 milljón króna</span>
					</div>
					<div class="info col-sm-3"></div>
					<div class="time col-sm-3">
						<span>11:07</span>
					</div>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span><span class="red">Jón Þór</span> keypti hlutabréf í <span class="red">CCP</span> fyrir 1,5 milljón króna</span>
					</div>
					<div class="info col-sm-3"></div>
					<div class="time col-sm-3">
						<span>14:07</span>
					</div>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span><span class="red">Jón Þór</span> keypti hlutabréf í <span class="red">Flemann</span> fyrir 3,5 milljón króna</span>
					</div>
					<div class="info col-sm-3"></div>
					<div class="time col-sm-3">
						<span>14:07</span>
					</div>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span><span class="red">Þorsteinn</span> keypti hlutabréf í <span class="red">Hlöllabátum</span> fyrir 500 þúsund krónur</span>
					</div>
					<div class="info col-sm-3"></div>
					<div class="time col-sm-3">
						<span>14:07</span>
					</div>
				</div>
				<div class="head">
					<h4>03.06.2016</h4>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span><span class="red">Þorsteinn</span> keypti hlutabréf í <span class="red">Reginn</span> fyrir 1 milljón króna</span>
					</div>
					<div class="info col-sm-3"></div>
					<div class="time col-sm-3">
						<span>15:47</span>
					</div>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span><span class="red">Óttar</span> keypti hlutabréf í <span class="red">TM</span> fyrir 2 milljón króna</span>
					</div>
					<div class="info col-sm-3"></div>
					<div class="time col-sm-3">
						<span>13:07</span>
					</div>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span><span class="red">Jón Þór</span> keypti hlutabréf í <span class="red">Össur</span> fyrir 2,5 milljón króna</span>
					</div>
					<div class="info col-sm-3"></div>
					<div class="time col-sm-3">
						<span>12:32</span>
					</div>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span><span class="red">Óttar</span> keypti hlutabréf í <span class="red">CCP</span> fyrir 500 þúsund krónur</span>
					</div>
					<div class="info col-sm-3"></div>
					<div class="time col-sm-3">
						<span>11:07</span>
					</div>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span><span class="red">Jón Þór</span> keypti hlutabréf í <span class="red">HFJ 14 1</span> fyrir 3,5 milljón króna</span>
					</div>
					<div class="info col-sm-3"></div>
					<div class="time col-sm-3">
						<span>10:56</span>
					</div>
				</div>
				<div class="line row">
					<div class="headline col-sm-18 text-left">
						<span><span class="red">Þorsteinn</span> keypti hlutabréf í <span class="red">Síminn</span> fyrir 3 milljón króna</span>
					</div>
					<div class="info col-sm-3"></div>
					<div class="time col-sm-3">
						<span>08:07</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
