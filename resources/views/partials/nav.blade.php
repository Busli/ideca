
<link rel="stylesheet" type="text/css" href="{{asset('css/imagehover.min.css')}}">
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.5/TweenMax.min.js"></script>
<script src="{{asset('js/navigation_manager.js')}}" defer></script>

<div class="navbar row">
	<div class="col-sm-4">
		<div class="inline-block">
			<a href="{{asset('profile/' . $user->id)}}">
				<img class="profile" data-id="{{$user->id}}" src="{{ asset('images/icons/avatar_icon.png') }}" alt="'Avatar'" />				
			</a>
		</div>
		<div id="dateDisplay" class="inline-block">
			<div id="navigationClock"></div>
			<div id="navigationDate"></div>
		</div>
	</div>
	<div class="col-sm-20">
		<nav>
			<a href="{{url('home')}}">
				<img src="{{ asset('images/icons/home_icon.png') }}">
				Fréttaveita
			</a>
			<a href="{{url('mynetwork')}}">
				<img src="{{ asset('images/icons/network_icon.png') }}">
				Tengslanet
			</a>
			<a href="{{url('portfolio')}}">
				<img src="{{ asset('images/icons/portfolio_icon.png') }}">
				Verðbréfasafn
			</a>
			<a href="{{url('market')}}">
				<img src="{{ asset('images/icons/market_icon.png') }}">
				Markaðurinn
			</a>
			<a href="{{url('topinvestors')}}">
				<img src="{{ asset('images/icons/topinvestor_icon.png') }}">
				Farsælustu Fjárfestarnir
			</a>
			<a href="{{url('logout')}}">
				<img class="logout" src="{{ asset('images/icons/logout_icon.png') }}">
				Útskráning
			</a>
		</nav>
	</div>
</div>