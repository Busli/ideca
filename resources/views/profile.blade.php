@extends('layouts.master')

@section('content')
@if( $userProfile )
<div class="row profile-container">
	<div class="col-sm-12">
		<p><span class="red">Nafn:</span> {{$userData->name}}</p>
		<p><span class="red">Notendanafn:</span> {{$user->username}}</p>
		<p><span class="red">Kennitala:</span> {{$userData->ssn}}</p>
		<p><span class="red">Heimilisfang:</span> {{$userData->address}}</p>
		<p><span class="red">Bæjarfélag:</span> {{$userData->city}}</p>
		<p><span class="red">Póstnúmer:</span> {{$userData->postal_code}}</p>
	</div>
	<div class="col-sm-12">
		<div class="profileContent">
			<div id="lastMonthGain">
				<div id="gain_Status"></div>
				<div id="gain_Own"></div>
				<div id="gain_Status"></div>
			</div>
			<div id="investments">
				<h3>Vörslureikningur</h3>
				<div id="Balance">Innistæða: {{ $balance != -1 ? number_format($balance, 0, '.','.') . ' ISK' : 'Enginn reikningur skráður.' }}</div>
				<div id="subFee">Fylgjendagjald: {{ number_format($subFee, 0, '.','.') . ' ISK' }}</div>
				<div id="">Eignir í hlutabréfum: {{ number_format($stocksValue, 0, '.','.') . ' ISK' }}</div>
				<div id="">Eignir í skuldabréfum: {{ number_format($bondsValue, 0, '.','.') . ' ISK' }}</div>
			</div>
			<div id="Payments">Gjöld: {{number_format($gjold, 0, '.','.') }} ISK</div>
			<div id="Gains">Tekjur: {{number_format($tekjur, 0, '.','.') }} ISK</div>
			<div id="UserSettings">
				<div id="followingPrice">
					<button class="button" id="user_followerFee">Skrá fylgjendagjald</button>
				</div>
				<div id="cardRegister">
					<button class="button" id="user_cardRegister">Skrá kortaupplýsingar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div id="stockChart">
		<div id="stockChart_Image">
			<div class="canvasContainer" >
        		<canvas id="myChart" width="100" height="100"></canvas>
    		</div>
		</div>
		<div id="stockChart_Info">
			<ul>
				<li>
					<p style="display:inline-block">Hlutabréf </p><li style="display:inline-block" id="pieChartInfo_hlutabref">{{$user_hlutabref}}</li><p style="display:inline-block">%</p>
				</li>

				<li>
					<p style="display:inline-block">Skuldabréf </p><li style="display:inline-block"  id="pieChartInfo_skuldabref">{{$user_skuldabref}}</li><p style="display:inline-block">%</p>
				</li>
			</ul>
		</div>
	</div>
</div>
@else
<div class="row">
 	<div class="col-xs-12 col-xs-offset-6">
 		<div class="not-following-container text-center">
	 		<h3>Þú ert ekki að fylgja {{$user->username}}</h3>
	 		<div class="follow-button">
	 			<button data-fee="{{$subscribeFee}}" data-id="{{$user->id}}" class="button follow" value="{{$user->username}}" data-bind="click: flollowButtonClick">Viltu fylgja {{$user->username}} fyrir {{number_format($subscribeFee, 0, '.','.')}} ISK?</button>
	 		</div>
	 	</div>
 	</div>
</div>
@endif
@stop

@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.min.js"></script>
<script src="{{asset('js/profile_manager.js')}}"></script>

@if($userProfile)
<script type="text/javascript">window.ideca.ProfileManager.initChart()</script>
@endif

@stop
