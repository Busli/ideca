@extends('layouts.master')

@section('content')
<div class="row my-network">
	<div class="col-sm-8 col-sm-offset-4 border-right">
		<div class="main-title">
			<h3>Þessir eru að fylgja þér</h3>
		</div>
		<div class="follower-line text-left">
			<div class="title">
				<p>Nafn</p>
			</div>
			<div class="line-content">
			@foreach ($iIsBeingFollowed as $follower)
				<div class="line">
					<a href="{{url('profile/' . $follower->user_id) }}"> {{ $follower['username'] }}</a>
				</div>
			@endforeach
			</div>
		</div>
	</div>
	<div class="col-sm-8">
		<div class="main-title">
			<h3>Þú ert að fylgja þessum</h3>
		</div>
		<div class="follower-line text-left">
			<div class="title">
				<p class="inline-block">Nafn</p>
				<p class="pull-right inline-block">Síðustu viðskipti</p>
			</div>
			<div class="line-content">
			@foreach ($iIsFollowing as $following)
				<div class="line">
					<a href="{{url('profile/' . $following->user_friend_id) }}"> {{ $following['username'] }}</a>
					<button class="button" style="width:60px;">Herma</button>
					<span class="pull-right" style="font-size:14px;margin-top:10px;">{{ $following->LastMovement }}</span>
				</div>
			@endforeach
			</div>
		</div>
	</div>
</div>
@stop
