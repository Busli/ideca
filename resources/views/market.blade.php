@extends('layouts.master')

@section('head')
	<link rel="stylesheet" type="text/css" href="{{asset('css/market.css')}}">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.min.js"></script>
  <script src="https://cdn.livemarketdata.com/KodiService.js" type="text/javascript"></script>

	<script src="{{asset('js/market_manager.js')}}" defer></script>
	<script src="{{asset('js/chart_manager.js')}}" defer></script>
@stop


@section('content')
<div class="canvasContainer" style="display:none" >
	<canvas id="myChart" width="400" height="400"></canvas>
</div>
	<div class="row">
	<div class="col-sm-24">
		<h2 style="text-align:center">Hlutabréf</h2>
			<table class="datagrid">
			    <thead>
						<tr style="border-bottom:2px solid #22BDA3">
			        <th style="padding:10px 5px">Nafn bréfs</th>
							<th style="padding:10px 5px">Gjaldmiðill</th>
							<th style="padding:10px 5px">Auðkenni</th>
							<th style="padding:10px 5px">Síðasta verð</th>
							<th style="padding:10px 5px">+/-</th>
							<th style="padding:10px 5px">%</th>
							<th style="padding:10px 5px">Kauptilboð</th>
							<th style="padding:10px 5px">Sölutilboð</th>
							<th style="padding:10px 10x">Heildarmagn</th>
							<th style="padding:10px 20px">Velta</th>
							<th style="width:150px:float:right"></th>
				    </tr>
					</thead>
			    <tbody data-bind="foreach:company">
			        <tr class="hlutabref_row">
			            <td  style="text-align:center" ><a href="#" data-bind="text: Name, click:$root.displayGraph "></a></td>
									<td style="text-align:center" data-bind="text: TradingCurrency"></td>
									<td data-bind="text: Symbol"></td>
									<td class="columnNumberAlign" data-bind="text: ClosingPrice"></td>
									<td class="columnNumberAlign" data-bind="text: IntraDayChanged"></td>
									<td class="columnNumberAlign" data-bind="text: LastPriceDiff"></td>
									<td class="columnNumberAlign" data-bind="text: BuyPrice"></td>
									<td class="columnNumberAlign" data-bind="text: SalePrice"></td>
									<td class="columnNumberAlign" data-bind="text: Volume"></td>
									<td class="columnNumberAlign" data-bind="text: Velta"></td>
									<td class="table-button-container"><button class="button" data-bind="click: $root.buyStock">Kaupa</button></td>
			        </tr>
			    </tbody>
			</table>
			<h2 style="text-align:center">Skuldabréf</h2>
				<table class="datagrid">
						<thead><tr style="border-bottom:2px solid #22BDA3">
								<th style="padding:10px 5px">Nafn</th>
								<th style="padding:10px 5px">Gjaldmiðill</th>
								<th style="padding:10px 5px">Flokkur</th>
								<th style="padding:10px 5px" >Kaupkrafa</th>
								<th style="padding:10px 5px">Kauptilboð</th>
								<th style="padding:10px 5px">Sölutilboð</th>
								<th style="padding:10px 5px">Síðasta verð</th>
								<th style="padding:10px 5px">%</th>
								<th style="padding:10px 5px">Velta</th>
								<th></th>
						</tr></thead>
						<tbody data-bind="foreach:bonds">
								<tr>
										<td style="text-align:center"> <a href="#" data-bind="text: Name, click:$root.displayGraph "></a></td>
										<td style="text-align:center" data-bind="text: TradingCurrency"></td>
										<td data-bind="text: Symbol"></td>
										<td  data-bind="text: LastYield" class="columnNumberAlign"> %</td>
										<td  data-bind="text: BuyPrice" class="columnNumberAlign"></td>
										<td  data-bind="text: SalePrice" class="columnNumberAlign"></td>
										<td  data-bind="text: ClosingPrice" class="columnNumberAlign"></td>
										<td data-bind="text: IntraDayChanged" class="columnNumberAlign"></td>
										<td  data-bind="text: Velta" class="columnNumberAlign"></td>
										<td class="table-button-container-red"><button class="button" data-bind="click: $root.buyBond">Kaupa</button></td>
								</tr>
						</tbody>
				</table>
    <!--<iframe style="width:100%;height:700px" src="http://www.keldan.is"></iframe>-->
		<div id="bla"></div>
  </div>
  </div>
@stop
