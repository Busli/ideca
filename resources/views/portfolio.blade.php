@extends('layouts.master')
@section('head')

	<link rel="stylesheet" type="text/css" href="{{asset('css/portfolio.css')}}">
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.min.js"></script>
	<script src="https://cdn.livemarketdata.com/KodiService.js" type="text/javascript"></script>
	<script src="{{asset('js/portfolio_manager.js')}}" defer></script>
@stop
@section('content')
	<div id="portfolioContent">

		<div id="current_hlutabref">
			<table class="portfolioTable">
				<caption><h1>Skuldabréf</h1></caption>
				<thead>
				<tr>
					<th class="cmpCode">Auðkenni</th>
					<th class="company">Nafn</th>
					<th class="shares">Hlutir</th>
					<th class="shares">Virði</th>
					<th class="movement">+/-</th>
					<th class="percent">%</th>
				</tr>
				</thead>
				<tbody>
					@foreach ($hlutabref as $temp)
						<tr><td class="hlutabref_symbol"> {{$temp->CompanySymbol }}</td><td> {{$temp->CompanyName }}</td><td class="hlutabref_shares">{{ $temp->Shares }}</td>
							<td class="hlutabref_now">{{ $temp->TotalValue }}</td><td class="hlutabref_move"> </td><td class="hlutabref_perc"> </td></tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div id="current_skuldabref">
			<table class="portfolioTable">
				<caption><h1>Hlutabréf</h1></caption>
				<thead>
				<tr>
					<th class="cmpCode">Auðkenni</th>
					<th class="company">Nafn</th>
					<th class="shares">Hlutir</th>
					<th class="shares">Virði</th>
					<th class="movement">+/-</th>
					<th class="percent">%</th>
				</tr>
				</thead>
				<tbody>
					@foreach ($skuldabref as $temp)
						<tr><td class="skuldabref_symbol"> {{$temp->CompanySymbol }}</td><td> {{$temp->CompanyName }}</td><td class="skuldabref_shares">{{ $temp->Shares }}</td>
							<td class="skuldabref_now">{{ $temp->TotalValue }}</td><td class="skuldabref_move"> </td><td class="skuldabref_perc"></td></tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div id="currentBusiness">
			<h1>Viðskipti í pöntun</h1>
			<div>
					<ul>
						<li>Kaup í Össur á 341.00</li>
						<li>Sala í Marel á 341.00</li>
					</ul>
			</div>
		</div>
		<!-- <div id="currentCopy">
			<h1>Hermanir í gangi</h1>
			<div>
				<ul>
					<li>Hermir Saga Guðmundsdóttir 3.500 kr</li>
				</ul>
			</div>
		</div> -->

	</div>
@stop
