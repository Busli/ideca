@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="col-xs-12 col-xs-offset-6">
			<form method="post" action="register">
				<span>Username</span>
				<input type="text" name="username" placeholder="User name">
				<span>E-mail</span>
				<input type="text" name="email" placeholder="E-mail">
                <span>Password</span>
				<input type="password" name="password" placeholder="Password">
                <span>SSN</span>
				<input type="text" name="ssn" placeholder="SSN">
				<button type="submit">Register</button>
			</form>
		</div>
	</div>
@stop
